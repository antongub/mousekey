package com.antongub

import com.antongub.view.MainView
import tornadofx.App

class MyApp: App(MainView::class, Styles::class)